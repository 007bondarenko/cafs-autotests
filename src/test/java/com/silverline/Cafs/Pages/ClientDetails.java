package com.silverline.Cafs.Pages;

import com.silverline.Cafs.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.lang.*;

public class ClientDetails extends BaseTests{

    public ClientDetails(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public WebDriver driver;

    //Data
    public double LN = Math.random();
    public double LN2;

    protected static String webClientManagerURL = "https://cafs--test.lightning.force.com/lightning/n/Client_Manager";
    public static String ErrorMessageText = "Required fields are missing: [Last Name]";

    public static String FirstName = "Test";
    public static String LastName = "User";
    public static String Email = "test.user@mail.com";
    public static String HomePhone = "0123456789";
    public static String HomeStreet = "testStreet";
    public static String HomeCity = "testCity";
    public static String HomeZip = "03162";
    public static String Birthdate = "May 25, 1990";
    public static String BirthdatePageTwo = "1990-05-25";
    public static String Household = "Barber - 2904 S Mason";
    public static String TaxBracket = "2";
    public static String RetirementDate = "May 10, 2000";
    public static String RetirementDatePageTwo = "2000-05-10";
    public static String TotalLifeInsurance = "1500";
    public static String PrimaryObjectiveOther = "POOtestText";
    public static String PoliticalOrganization = "121345test";
    public static String AffiliationFirmName = "SuperFirm";
    public static String AffiliationCompanySymbols = "SuperSimbol";
    public static String AffiliationPositionEmployed = "SuperPosition";

    //---Data From Picklists
    public static String Solutation = "Ms.";
    public static String HomeCountry = "USA";
    public static String TotalNetWorth = "100001 - 200000";
    public static String LiquidNetWorth = "200001 - 500000";
    public static String AnnualIncome = "300001 - 500000";
    public static String DefaultRiskTolerance = "MEDIUM";
    public static String DefaultLiquidityNeeds = "VERY_IMPORTANT";
    public static String DefaultTimeHorizon = "AVERAGE";
    public static String PrimaryObjective = "OTHER";
    public static String FinancialPlanningExperience = "GOOD";
    public static String ControlPerson = "NO";

    //---FIELDS
    @FindBy(xpath = "//div[@class='slds-col slds-p-around_small slds-size_1-of-2'][1]/div/select")
    public WebElement SolutationPicklist;

    @FindBy(id ="input-3")
    public WebElement FirstNameField;

    @FindBy(id ="input-4")
    public WebElement LastNameField;

    @FindBy(id ="input-5")
    public WebElement EmailField;

    @FindBy(id ="input-6")
    public WebElement HomePhoneField;

    @FindBy(id ="input-7")
    public WebElement HomeStreetField;

    @FindBy(id ="input-8")
    public WebElement HomeCityField;

    @FindBy(xpath ="/html//form[@id='form-wrapper']/div/div[8]/div/select")
    public WebElement HomeStatePicklist;

    @FindBy(id ="input-9")
    public WebElement HomeZipField;

    @FindBy(xpath ="/html//form[@id='form-wrapper']/div/div[10]/div/select")
    public WebElement HomeCountryPicklist;

    @FindBy(id ="input-27")
    public WebElement BirthdateField;

    @FindBy(xpath ="/html//form[@id='form-wrapper']/div[@class='slds-grid slds-wrap']/div[12]/div//input[@type='text']")
    public WebElement HouseholdField;

    @FindBy(css =".cSL_cmp_InputLookupElement .slds-media__body")
    public WebElement HouseholdFirstValue;

    @FindBy(xpath ="//label[@for ='input-11']/span[@class='slds-checkbox_faux']")
    public WebElement NewHouseholdCheckbox;

    @FindBy(xpath ="/html//form[@id='form-wrapper']/div/div[14]/div/select")
    public WebElement TotalNewWorthPicklist;

    @FindBy(xpath ="/html//form[@id='form-wrapper']/div/div[15]/div/select")
    public WebElement LiquidNetWorthPicklist;

    @FindBy(xpath ="/html//form[@id='form-wrapper']/div/div[16]/div/select")
    public WebElement AnnualIncomePicklist;

    @FindBy(id ="input-12")
    public WebElement TaxBracketField;

    @FindBy(id ="input-28")
    public WebElement RetirementDateField;

    @FindBy(id ="input-14")
    public WebElement TotalLifeInsuranceField;

    @FindBy(xpath ="/html//form[@id='form-wrapper']/div/div[20]/div/select")
    public WebElement DefaultRiskTolerancePicklist;

    @FindBy(xpath ="/html//form[@id='form-wrapper']/div/div[21]/div/select")
    public WebElement DefaultLiquidityNeedsPicklist;

    @FindBy(xpath ="/html//form[@id='form-wrapper']/div/div[22]/div/select")
    public WebElement DefaultTimeHorizonPicklist;

    @FindBy(xpath ="/html//form[@id='form-wrapper']/div/div[23]/div/select")
    public WebElement PrimaryObjectivePicklist;

    @FindBy(id ="input-15")
    public WebElement PrimaryObjectiveOtherField;

    @FindBy(xpath ="/html//form[@id='form-wrapper']/div/div[25]/div/select")
    public WebElement FinancialPlanningExperiencePicklist;

    @FindBy(id ="input-16")
    public WebElement PoliticalOrganizationField;

    @FindBy(xpath="//label[@for ='input-17']/span[@class='slds-checkbox_faux']")
    public WebElement PoliticallyExposedCheckbox;

    @FindBy(xpath ="//label[@for ='input-18']/span[@class='slds-checkbox_faux']")
    public WebElement FINRACheckbox;

    @FindBy(id ="input-19")
    public WebElement AffiliationFirmNameField;

    @FindBy(id ="input-20")
    public WebElement AffiliationCompanySymbolsField;

    @FindBy(id ="input-21")
    public WebElement AffiliationPositionEmployedField;

    @FindBy(xpath ="/html//form[@id='form-wrapper']/div/div[32]/div/select")
    public WebElement ControlPersonPicklist;

    //---Fields Page Two
    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][1]/p")
    public WebElement SolutationDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][2]/p")
    public WebElement FirstNameDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][3]/p")
    public WebElement LastNameDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][4]/p")
    public WebElement EmailDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][5]/p")
    public WebElement HomePhoneDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][6]/p")
    public WebElement HomeStreetDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][7]/p")
    public WebElement HomeCityDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][8]/p")
    public WebElement HomeStateDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][9]/p")
    public WebElement HomeZipDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][10]/p")
    public WebElement HomeCountryDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][11]/p")
    public WebElement BirthdateDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][12]/p")
    public WebElement HouseholdDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][13]/p")
    public WebElement CreateNewHouseholdDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][14]/p")
    public WebElement TotalNetWorthDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][15]/p")
    public WebElement LiquidNetWorthDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][16]/p")
    public WebElement AnnualIncomeDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][17]/p")
    public WebElement TaxBracketDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][18]/p")
    public WebElement RetirementDateDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][19]/p")
    public WebElement TotalLifeInsuranceDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][20]/p")
    public WebElement DefaultRiskToleranceDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][21]/p")
    public WebElement DefaultLiquidityNeedsDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][22]/p")
    public WebElement DefaultTimeHorizonDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][23]/p")
    public WebElement PrimaryObjectiveDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][24]/p")
    public WebElement PrimaryObjectiveOtherDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][25]/p")
    public WebElement FinancialPlanningExperienceDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][26]/p")
    public WebElement PoliticalOrganizationDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][27]/p")
    public WebElement PoliticallyExposedDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][28]/p")
    public WebElement FINDADataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][29]/p")
    public WebElement AffiliationFirmNameDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][30]/p")
    public WebElement AffiliationCompanySymbolsDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][31]/p")
    public WebElement AffiliationPositionEmployedDataPageTwo;

    @FindBy(xpath ="//div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][32]/p")
    public WebElement ControlPersonDataPageTwo;

    //---Values from Pick lists
    @FindBy(xpath = "//div[@class='slds-col slds-p-around_small slds-size_1-of-2'][1]/div/label/span")
    public static WebElement SolutationFieldName;

    @FindBy(xpath = "//div[@class='slds-col slds-p-around_small slds-size_1-of-2'][1]/div/select/option[1]")
    private WebElement SolutationValueNone;

    @FindBy(xpath = "//div[@class='slds-col slds-p-around_small slds-size_1-of-2'][1]/div/select/option[2]")
    public WebElement SolutationValueMr;

    @FindBy(xpath = "//div[@class='slds-col slds-p-around_small slds-size_1-of-2'][1]/div/select/option[3]")
    private WebElement SolutationValueMs;

    @FindBy(xpath = "//div[@class='slds-col slds-p-around_small slds-size_1-of-2'][1]/div/select/option[4]")
    private WebElement SolutationValueMrs;

    @FindBy(xpath = "//div[@class='slds-col slds-p-around_small slds-size_1-of-2'][1]/div/select/option[5]")
    private WebElement SolutationValueDr;

    @FindBy(xpath = "//div[@class='slds-col slds-p-around_small slds-size_1-of-2'][1]/div/select/option[6]")
    private WebElement SolutationValueProf;

    //---BUTTONS
    @FindBy(xpath = "//div[@class=\"SL_cmp_AccordionElement SL_cmp_AccordionElement-isOpen cSL_cmp_AccordionElement\"][1]/div/form/button[@type=\"submit\"][1]")
    public WebElement SubmitButtonOnClientDetailsPage;

    @FindBy(xpath = "//div[@id='brandBand_1']/div/div//div[@class='cSL_cmp_AccordionForm']/div/div[1]/div[@class='SL_cmp_AccordionElementFooter']/button[2]")
    public WebElement NextButtonOnClientDetailsPage;

    @FindBy(xpath = "//button[@class='slds-button slds-button_neutral SL_cmp_AccordionElementButton SL_cmp_AccordionElementButton-next'][1]")
    public WebElement NextButtonOnClientDetailsPageTwo;

    //---METHODS
    public void GoToClientDetailsPage(){
        driver.get(webClientManagerURL);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SolutationPicklist.isDisplayed();
    }

    public void FillFieldsOnClientDetailsPage() throws InterruptedException {
        LN2 = LN;
        mm.SelectFromPicklist(cd.SolutationPicklist, 2);
        mm.FillField(cd.FirstNameField, cd.FirstName);
        mm.FillField(cd.LastNameField, cd.LastName+LN2);
        mm.FillField(cd.EmailField, cd.Email);
        mm.FillField(cd.HomePhoneField, cd.HomePhone);
        mm.FillField(cd.HomeStreetField, cd.HomeStreet);
        mm.FillField(cd.HomeCityField, cd.HomeCity);
        mm.SelectFromPicklist(cd.HomeStatePicklist, 5);
        mm.FillField(cd.HomeZipField, cd.HomeZip);
        mm.SelectFromPicklist(cd.HomeCountryPicklist, 1);
        mm.FillField(cd.BirthdateField, cd.Birthdate);
        mm.SelectFromSearchField(cd.HouseholdField, cd.HouseholdFirstValue , cd.Household);
        //mm.ClickOn(cd.NewHouseholdCheckbox);
        mm.SelectFromPicklist(cd.TotalNewWorthPicklist, 3);
        mm.SelectFromPicklist(cd.LiquidNetWorthPicklist, 4);
        mm.SelectFromPicklist(cd.AnnualIncomePicklist, 6);
        mm.FillField(cd.TaxBracketField, cd.TaxBracket);
        mm.FillField(cd.RetirementDateField, cd.RetirementDate);
        mm.FillField(cd.TotalLifeInsuranceField, cd.TotalLifeInsurance);
        mm.SelectFromPicklist(cd.DefaultRiskTolerancePicklist, 2);
        mm.SelectFromPicklist(cd.DefaultLiquidityNeedsPicklist, 1);
        mm.SelectFromPicklist(cd.DefaultTimeHorizonPicklist, 2);
        mm.SelectFromPicklist(cd.PrimaryObjectivePicklist, 5);
        mm.FillField(cd.PrimaryObjectiveOtherField, cd.PrimaryObjectiveOther);
        mm.SelectFromPicklist(cd.FinancialPlanningExperiencePicklist, 3);
        mm.FillField(cd.PoliticalOrganizationField, cd.PoliticalOrganization);
        mm.ClickOn(cd.PoliticallyExposedCheckbox);
        mm.ClickOn(cd.FINRACheckbox);
        mm.FillField(cd.AffiliationFirmNameField, cd.AffiliationFirmName);
        mm.FillField(cd.AffiliationCompanySymbolsField, cd.AffiliationCompanySymbols);
        mm.FillField(cd.AffiliationPositionEmployedField, cd.AffiliationPositionEmployed);
        mm.SelectFromPicklist(cd.ControlPersonPicklist, 1);
    }

    public void CheckFieldsOnClientDetailsPage(){
        mm.CheckText(cd.SolutationDataPageTwo, cd.Solutation);
        mm.CheckText(cd.FirstNameDataPageTwo, cd.FirstName);
        mm.CheckText(cd.LastNameDataPageTwo, cd.LastName+LN2);
        mm.CheckText(cd.EmailDataPageTwo, cd.Email);
        mm.CheckText(cd.HomePhoneDataPageTwo, cd.HomePhone);
        mm.CheckText(cd.HomeStreetDataPageTwo, cd.HomeStreet);
        mm.CheckText(cd.HomeCityDataPageTwo, cd.HomeCity);
        mm.CheckText(cd.HomeZipDataPageTwo, cd.HomeZip);
        mm.CheckText(cd.HomeCountryDataPageTwo, cd.HomeCountry);
        mm.CheckText(cd.BirthdateDataPageTwo, cd.BirthdatePageTwo);
        mm.CheckText(cd.HouseholdDataPageTwo, cd.Household);
        mm.CheckText(cd.TotalNetWorthDataPageTwo, cd.TotalNetWorth);
        mm.CheckText(cd.LiquidNetWorthDataPageTwo, cd.LiquidNetWorth);
        mm.CheckText(cd.AnnualIncomeDataPageTwo, cd.AnnualIncome);
        mm.CheckText(cd.TaxBracketDataPageTwo, cd.TaxBracket);
        mm.CheckText(cd.RetirementDateDataPageTwo, cd.RetirementDatePageTwo);
        mm.CheckText(cd.TotalLifeInsuranceDataPageTwo, cd.TotalLifeInsurance);
        mm.CheckText(cd.DefaultRiskToleranceDataPageTwo, cd.DefaultRiskTolerance);
        mm.CheckText(cd.DefaultLiquidityNeedsDataPageTwo, cd.DefaultLiquidityNeeds);
        mm.CheckText(cd.DefaultTimeHorizonDataPageTwo, cd.DefaultTimeHorizon);
        mm.CheckText(cd.PrimaryObjectiveDataPageTwo, cd.PrimaryObjective);
        mm.CheckText(cd.PrimaryObjectiveOtherDataPageTwo, cd.PrimaryObjectiveOther);
        mm.CheckText(cd.FinancialPlanningExperienceDataPageTwo, cd.FinancialPlanningExperience);
        mm.CheckText(cd.PoliticalOrganizationDataPageTwo, cd.PoliticalOrganization);
        mm.CheckText(cd.AffiliationFirmNameDataPageTwo, cd.AffiliationFirmName);
        mm.CheckText(cd.AffiliationCompanySymbolsDataPageTwo, cd.AffiliationCompanySymbols);
        mm.CheckText(cd.AffiliationPositionEmployedDataPageTwo, cd.AffiliationPositionEmployed);
        mm.CheckText(cd.ControlPersonDataPageTwo, cd.ControlPerson);
    }
}
