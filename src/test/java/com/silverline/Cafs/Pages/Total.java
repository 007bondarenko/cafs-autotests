package com.silverline.Cafs.Pages;

import com.silverline.Cafs.BaseTests.BaseTests;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Total extends BaseTests{

    public WebDriver driver;

    public Total(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(css = ".slds-accordion__section.slds-is-open")
    public WebElement TotalBlock;

    //---Block One
    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][1]/p")
    public WebElement SalutationTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][2]/p")
    public WebElement FirstNameTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][3]/p")
    public WebElement LastNameTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][4]/p")
    public WebElement EmailTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][5]/p")
    public WebElement HomePhoneTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][6]/p")
    public WebElement HomeStreetTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][7]/p")
    public WebElement HomeCityTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][8]/p")
    public WebElement HomeStateTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][9]/p")
    public WebElement HomeZipTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][10]/p")
    public WebElement HomeCountryTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][11]/p")
    public WebElement BirthdateTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][12]/p")
    public WebElement HouseholdTotalPage1;

    //@FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][13]/p")
    //public WebElement NewHouseholdTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][14]/p")
    public WebElement TotalNetWorthTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][15]/p")
    public WebElement LiquidNetWorthTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][16]/p")
    public WebElement AnnualIncomeTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][17]/p")
    public WebElement TaxBracketTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][18]/p")
    public WebElement RetirementDateTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][19]/p")
    public WebElement TotalLifeInsuranceTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][20]/p")
    public WebElement DefaultRiskToleranceTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][21]/p")
    public WebElement DefaultLiquidityNeedsTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][22]/p")
    public WebElement DefaultTimeHorizonTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][23]/p")
    public WebElement PrimaryObjectiveTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][24]/p")
    public WebElement PrimaryObjectiveOtherTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][25]/p")
    public WebElement FinancialPlanningExperienceTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][26]/p")
    public WebElement PoliticalOrganizationTotalPage1;

    //@FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][27]/p")
    //public WebElement PoliticallyExposedTotalPage1;

    //@FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][28]/p")
    //public WebElement FINRATotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][29]/p")
    public WebElement AffiliationFirmNameTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][30]/p")
    public WebElement AffiliationCompanySymbolsTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][31]/p")
    public WebElement AffiliationPositionEmployedTotalPage1;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][1]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][32]/p")
    public WebElement ControlPersonTotalPage1;

    //---Block Two
    @FindBy(css = ".slds-accordion__list-item:nth-child(2) .slds-button.slds-button_reset.slds-accordion__summary-action")
    public WebElement TotalPageOpenBlockTwoButton;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][2]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][1]/p")
    public WebElement RecordTypeIDTotalPage2;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][2]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][2]/p")
    public WebElement CarrierTotalPage2;

    //@FindBy(xpath = "//li[@class='slds-accordion__list-item '][2]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][3]/p")
    //public WebElement ProductTotalPage2;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][2]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][4]/p")
    public WebElement NameTotalPage2;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][2]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][5]/p")
    public WebElement LegalEntityNameTotalPage2;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][2]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][6]/p")
    public WebElement CloseDateTotalPage2;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][2]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][7]/p")
    public WebElement StageTotalPage2;

    //---Block Three
    @FindBy(css = ".slds-accordion__list-item:nth-child(3) .slds-button.slds-button_reset.slds-accordion__summary-action")
    public WebElement TotalPageOpenBlockThreeButton;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][3]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][1]/p")
    public WebElement OpportunityTotalPage3;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][3]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][2]/p")
    public WebElement LegalEntityTotalPage3;

    @FindBy(xpath = "//li[@class='slds-accordion__list-item '][3]/section/div/div/div[@class='slds-p-bottom--small slds-horizontal--small slds-col slds-size_1-of-1 slds-large-size_1-of-2'][3]/p")
    public WebElement RoleTotalPage3;

    //---METHODS
    public void CheckDataOnTotalPage(){
        SalutationTotalPage1.isDisplayed();
        CheckDataOnTotalPageBlockOne();
        CheckDataOnTotalPageBlockTwo();
        CheckDataOnTotalPageBlockThree();
    }

    public void CheckDataOnTotalPageBlockOne(){
        SalutationTotalPage1.isDisplayed();
        mm.CheckText(SalutationTotalPage1, cd.Solutation);
        mm.CheckText(FirstNameTotalPage1, cd.FirstName);
        mm.CheckText(LastNameTotalPage1, cd.LastName+cd.LN2);
        mm.CheckText(EmailTotalPage1, cd.Email);
        mm.CheckText(HomePhoneTotalPage1, cd.HomePhone);
        mm.CheckText(HomeStreetTotalPage1, cd.HomeStreet);
        mm.CheckText(HomeCityTotalPage1, cd.HomeCity);
        mm.CheckText(HomeZipTotalPage1, cd.HomeZip);
        mm.CheckText(HomeCountryTotalPage1, cd.HomeCountry);
        mm.CheckText(BirthdateTotalPage1, cd.BirthdatePageTwo);
        mm.CheckText(HouseholdTotalPage1, cd.Household);
        mm.CheckText(TotalNetWorthTotalPage1, cd.TotalNetWorth);
        mm.CheckText(LiquidNetWorthTotalPage1, cd.LiquidNetWorth);
        mm.CheckText(AnnualIncomeTotalPage1, cd.AnnualIncome);
        mm.CheckText(TaxBracketTotalPage1, cd.TaxBracket);
        mm.CheckText(RetirementDateTotalPage1, cd.RetirementDatePageTwo);
        mm.CheckText(TotalLifeInsuranceTotalPage1, cd.TotalLifeInsurance);
        mm.CheckText(DefaultRiskToleranceTotalPage1, cd.DefaultRiskTolerance);
        mm.CheckText(DefaultLiquidityNeedsTotalPage1, cd.DefaultLiquidityNeeds);
        mm.CheckText(DefaultTimeHorizonTotalPage1, cd.DefaultTimeHorizon);
        mm.CheckText(PrimaryObjectiveTotalPage1, cd.PrimaryObjective);
        mm.CheckText(PrimaryObjectiveOtherTotalPage1, cd.PrimaryObjectiveOther);
        mm.CheckText(FinancialPlanningExperienceTotalPage1, cd.FinancialPlanningExperience);
        mm.CheckText(PoliticalOrganizationTotalPage1, cd.PoliticalOrganization);
        mm.CheckText(AffiliationFirmNameTotalPage1, cd.AffiliationFirmName);
        mm.CheckText(AffiliationCompanySymbolsTotalPage1, cd.AffiliationCompanySymbols);
        mm.CheckText(AffiliationPositionEmployedTotalPage1, cd.AffiliationPositionEmployed);
        mm.CheckText(ControlPersonTotalPage1, cd.ControlPerson);
    }

    public void CheckDataOnTotalPageBlockTwo(){
        TotalPageOpenBlockTwoButton.sendKeys(Keys.ENTER);
        RecordTypeIDTotalPage2.isDisplayed();
        mm.CheckText(RecordTypeIDTotalPage2, opp.RecordType);
        mm.CheckText(CarrierTotalPage2, opp.Carrier);
        mm.CheckText(NameTotalPage2, opp.Name);
        mm.CheckText(LegalEntityNameTotalPage2, cd.FirstName+" "+cd.LastName+cd.LN2);
        mm.CheckText(CloseDateTotalPage2, opp.CloseDateTotalPage);
        mm.CheckText(StageTotalPage2, opp.StageTotalPage);

    }

    public void CheckDataOnTotalPageBlockThree(){
        TotalPageOpenBlockThreeButton.sendKeys(Keys.ENTER);
        OpportunityTotalPage3.isDisplayed();
        mm.CheckText(OpportunityTotalPage3, opp.Name);
        mm.CheckText(LegalEntityTotalPage3, cd.FirstName+" "+cd.LastName+cd.LN2);
        mm.CheckText(RoleTotalPage3, opp.RoleTotalPage);
    }
}
