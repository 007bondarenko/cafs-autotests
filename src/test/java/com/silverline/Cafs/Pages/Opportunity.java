package com.silverline.Cafs.Pages;

import com.silverline.Cafs.BaseTests.BaseTests;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Opportunity extends BaseTests{
    public WebDriver driver;

    public Opportunity(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public static String RecordType = "APEX - Brokerage Account";
    public static String Carrier = "Allianz";
    public static String Name = "TestOpportunity";
    public static String CloseDate = "Jun 10, 2020";
    public static String CloseDate2 = "Jun 21, 2019";
    public static String CloseDateTotalPage = "2020-06-10";
    public static String StageTotalPage = "Proposal";
    public static String RoleTotalPage = "Beneficiary";

    public static String ErrorMessageForRecordType = "Record Type ID: this ID value isn't valid for the user:";
    public static String ErrorMessageForStage = "Required fields are missing: [Stage]";

    //---FIELDS
    @FindBy(xpath ="//div[@class='cSL_cmp_Accordion']/div[3]//form[@id='form-wrapper']/div/div[1]/div//input[@type='text']")
    public WebElement RecordTypeField;

    @FindBy(xpath ="//li[@class='slds-listbox__item cSL_cmp_InputLookupElement']/span/span/span[@class='slds-listbox__option-text slds-listbox__option-text_entity']")
    public WebElement RecordTypeFirstValue;

    @FindBy(css =".slds-size_1-of-2:nth-child(2) .uiInput--input")
    public WebElement CarrierField;

    @FindBy(css =".slds .slds-listbox__item.cSL_cmp_InputLookupElement .slds-media__body .slds-listbox__option-text_entity")
    public WebElement CarrierFirstValue;

    @FindBy(css =".slds-size_1-of-2:nth-child(3) .uiInput--input")
    public WebElement ProductField;

    @FindBy(id ="input-25")
    public WebElement NameField;

    @FindBy(xpath ="//span[@class='slds-pill__label']")
    public WebElement LegalEntityNameField;

    @FindBy(id ="input-29")
    public WebElement CloseDateField;

    @FindBy(css =".slds-size_1-of-2:nth-child(7) .slds-select.select")
    public WebElement StageField;

    //---BUTTONS
    @FindBy(xpath ="//div[@class='cSL_cmp_Accordion']/div[@class='SL_cmp_AccordionElement SL_cmp_AccordionElement-isOpen cSL_cmp_AccordionElement']/div/form[@id='form-wrapper']/button")
    public WebElement SmallSubmitButtonOnOpportunityPage;

    @FindBy(xpath ="//div[@id='brandBand_1']/div/div[1]//div[@class='cSL_cmp_AccordionForm']/div/div[3]/div[@class='SL_cmp_AccordionElementFooter']/button[1]")
    public WebElement PreviousButtonOnOpportunityPage;

    @FindBy(xpath ="//div[@class='cSL_cmp_AccordionForm']/div/div[3]/div[@class='SL_cmp_AccordionElementFooter']/button[3]")
    public WebElement BigSubmitButtonOnOpportunityPage;

    //---Opportunity Roles
    @FindBy(css =".cSL_cmp_DynamicForm:nth-child(3) .uiInput--input")
    public WebElement LegalEntityField;

    @FindBy(css =".cSL_cmp_InputLookupElement .slds-listbox__option-text.slds-listbox__option-text_entity")
    public WebElement LegalEntityFirstValue;

    @FindBy(css =".cSL_cmp_DynamicForm:nth-child(3) .slds-select.select")
    public WebElement RolePicklist;

    @FindBy(css =".cSL_cmp_DynamicForm:nth-child(3) .slds-button.slds-button_neutral.slds-m-vertical--medium")
    public WebElement SubmitButtonOnOpportunityRoleBlock;

    @FindBy(css =".slds-accordion__section.slds-is-open:nth-child(1)")
    public WebElement NewOpportunityRoleDataBlock;

    //---Errors
    @FindBy(xpath ="div[@id='input-25-message']")
    public WebElement ErrorMessageForNameField;


    //---METHODS
    public void GoToOpportunityPage(){
        bh.GoToBuildHouseholdPage();
        mm.NavigateToNextPage(bh.NextButtonOnBuildHouseholdPage, opp.RecordTypeField);
    }

    public void FillFieldsOnOpportunityPage() throws InterruptedException {
        mm.SelectFromSearchField(RecordTypeField, RecordTypeFirstValue, RecordType);
        mm.SelectFromSearchField(CarrierField, CarrierFirstValue, Carrier);
        mm.FillField(NameField, Name);
        //mm.CheckText(opp.LegalEntityNameField, cd.FirstName+" "+cd.LastName+LN2);
        mm.FillField(CloseDateField, CloseDate);
        mm.SelectFromPicklist(StageField, 2);
    }

    public void CreateNewOpportunityRole() throws InterruptedException {
        opp.RolePicklist.sendKeys(Keys.ENTER);
        mm.SelectFromSearchField(opp.LegalEntityField, opp.LegalEntityFirstValue, cd.FirstName+" "+cd.LastName+cd.LN2);
        mm.SelectFromPicklist(opp.RolePicklist, 3);
        opp.SubmitButtonOnOpportunityRoleBlock.sendKeys(Keys.ENTER);
        opp.NewOpportunityRoleDataBlock.isDisplayed();
    }
}
