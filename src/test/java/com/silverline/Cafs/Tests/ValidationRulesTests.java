package com.silverline.Cafs.Tests;

import com.silverline.Cafs.BaseTests.BaseTests;
import org.openqa.selenium.Keys;
import org.junit.Test;

public class ValidationRulesTests extends BaseTests{

    @Test
    public void CheckValidationOnClientDetailsPage(){
        System.out.println("[INFO]: Test 'Check Validation On Client Details Page' started >>>");
        cd.GoToClientDetailsPage();
        //--->>>Check Error message
        System.out.println("[INFO]: Submit empty 'Client Details' form.");
        cd.SubmitButtonOnClientDetailsPage.sendKeys(Keys.ENTER);
        System.out.println("[INFO]: Check message for required field ('Last Name').");
        mm.CheckErrorMessage(cd.ErrorMessageText);
        System.out.println("[INFO]: Test 'Check Validation On Client Details Page' - PASSED.");
    }

    @Test
    public void CheckValidationOnBuildHouseholdPage(){
        System.out.println("[INFO]: Test 'Check Validation On Build Household Page' started >>>");
        bh.GoToBuildHouseholdPage();
        //--->>>Check Error message
        System.out.println("[INFO]: Submit empty 'Build Household' form.");
        bh.SubmitButtonOnBuildHouseholdPage.sendKeys(Keys.ENTER);
        System.out.println("[INFO]: Check message for required field ('Related Legal Entity').");
        mm.CheckErrorMessage(bh.ErrorMessageForRelatedLegalEntity);
        System.out.println("[INFO]: Test 'Check Validation On Build Household Page' - PASSED.");
    }

    @Test
    public void CheckValidationOnOpportunityPage() throws InterruptedException {
        System.out.println("[INFO]: Test 'Check Validation On Opportunity Page' started >>>");
        opp.GoToOpportunityPage();
        mm.FillField(opp.NameField, opp.Name);
        mm.FillField(opp.CloseDateField, opp.CloseDate);
        System.out.println("[INFO]: Submit 'Opportunity' form without required field 'Record Type'.");
        opp.SmallSubmitButtonOnOpportunityPage.sendKeys(Keys.ENTER);
        //--->>>Check Error message
        System.out.println("[INFO]: Check message for required field ('Record Type').");
        mm.CheckErrorMessage(opp.ErrorMessageForRecordType);
        mm.SelectFromSearchField(opp.RecordTypeField, opp.RecordTypeFirstValue, opp.RecordType);
        System.out.println("[INFO]: Submit 'Opportunity' form without required field 'Stage'.");
        opp.SmallSubmitButtonOnOpportunityPage.sendKeys(Keys.ENTER);
        //--->>>Check Error message
        System.out.println("[INFO]: Check message for required field ('Stage').");
        mm.CheckErrorMessage(opp.ErrorMessageForStage);
        System.out.println("[INFO]: Test 'Check Validation On Opportunity Page' - PASSED.");
        //mm.SelectFromPicklist(opp.StageField, 1);
        //opp.SmallSubmitButtonOnOpportunityPage.sendKeys(Keys.ENTER);
        //opp.LegalEntityField.isDisplayed();
    }
}
