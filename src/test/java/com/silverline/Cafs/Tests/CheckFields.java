package com.silverline.Cafs.Tests;

import com.silverline.Cafs.BaseTests.BaseTests;
import org.junit.Assert;
import org.junit.Test;

public class CheckFields extends BaseTests{

    @Test
    public void CheckFieldsOnTheClientDetailsPage(){
        System.out.println("[INFO]: Test 'Check Fields On The Client Details Page' started >>>");
        cd.GoToClientDetailsPage();
        Assert.assertTrue("'SolutationPicklist' field is not displayed, or disabled.", mm.IsElementPresent(cd.SolutationPicklist));
        Assert.assertTrue("'FirstNameField' field is not displayed, or disabled.", mm.IsElementPresent(cd.FirstNameField));
        Assert.assertTrue("'LastNameField' field is not displayed, or disabled.", mm.IsElementPresent(cd.LastNameField));
        Assert.assertTrue("'EmailField' field is not displayed, or disabled.", mm.IsElementPresent(cd.EmailField));
        Assert.assertTrue("'HomePhoneField' field is not displayed, or disabled.", mm.IsElementPresent(cd.HomePhoneField));
        Assert.assertTrue("'HomeStreetField' field is not displayed, or disabled.", mm.IsElementPresent(cd.HomeStreetField));
        Assert.assertTrue("'HomeCityField' field is not displayed, or disabled.", mm.IsElementPresent(cd.HomeCityField));
        Assert.assertTrue("'HomeStatePicklist' field is not displayed, or disabled.", mm.IsElementPresent(cd.HomeStatePicklist));
        Assert.assertTrue("'HomeZipField' field is not displayed, or disabled.", mm.IsElementPresent(cd.HomeZipField));
        Assert.assertTrue("'HomeCountryPicklist' field is not displayed, or disabled.", mm.IsElementPresent(cd.HomeCountryPicklist));
        Assert.assertTrue("'BirthdateField' field is not displayed, or disabled.", mm.IsElementPresent(cd.BirthdateField));
        Assert.assertTrue("'HouseholdField' field is not displayed, or disabled.", mm.IsElementPresent(cd.HouseholdField));
        Assert.assertTrue("'NewHouseholdCheckbox' checkbox is not displayed, or disabled.", mm.IsElementPresent(cd.NewHouseholdCheckbox));
        Assert.assertTrue("'TotalNewWorthPicklist' field is not displayed, or disabled.", mm.IsElementPresent(cd.TotalNewWorthPicklist));
        Assert.assertTrue("'LiquidNetWorthPicklist' field is not displayed, or disabled.", mm.IsElementPresent(cd.LiquidNetWorthPicklist));
        Assert.assertTrue("'AnnualIncomePicklist' field is not displayed, or disabled.", mm.IsElementPresent(cd.AnnualIncomePicklist));
        Assert.assertTrue("'TaxBracketField' field is not displayed, or disabled.", mm.IsElementPresent(cd.TaxBracketField));
        Assert.assertTrue("'RetirementDateField' field is not displayed, or disabled.", mm.IsElementPresent(cd.RetirementDateField));
        Assert.assertTrue("'TotalLifeInsuranceField' field is not displayed, or disabled.", mm.IsElementPresent(cd.TotalLifeInsuranceField));
        Assert.assertTrue("'DefaultRiskTolerancePicklist' field is not displayed, or disabled.", mm.IsElementPresent(cd.DefaultRiskTolerancePicklist));
        Assert.assertTrue("'DefaultLiquidityNeedsPicklist' field is not displayed, or disabled.", mm.IsElementPresent(cd.DefaultLiquidityNeedsPicklist));
        Assert.assertTrue("'DefaultLiquidityNeedsPicklist' field is not displayed, or disabled.", mm.IsElementPresent(cd.DefaultLiquidityNeedsPicklist));
        Assert.assertTrue("'DefaultTimeHorizonPicklist' field is not displayed, or disabled.", mm.IsElementPresent(cd.DefaultTimeHorizonPicklist));
        Assert.assertTrue("'PrimaryObjectivePicklist' field is not displayed, or disabled.", mm.IsElementPresent(cd.PrimaryObjectivePicklist));
        Assert.assertTrue("'PrimaryObjectiveOtherField' field is not displayed, or disabled.", mm.IsElementPresent(cd.PrimaryObjectiveOtherField));
        Assert.assertTrue("'FinancialPlanningExperiencePicklist' field is not displayed, or disabled.", mm.IsElementPresent(cd.FinancialPlanningExperiencePicklist));
        Assert.assertTrue("'PoliticalOrganizationField' field is not displayed, or disabled.", mm.IsElementPresent(cd.PoliticalOrganizationField));
        Assert.assertTrue("'PoliticallyExposedCheckbox' checkbox is not displayed, or disabled.", mm.IsElementPresent(cd.PoliticallyExposedCheckbox));
        Assert.assertTrue("'FINRACheckbox' checkbox is not displayed, or disabled.", mm.IsElementPresent(cd.FINRACheckbox));
        Assert.assertTrue("'AffiliationFirmNameField' field is not displayed, or disabled.", mm.IsElementPresent(cd.AffiliationFirmNameField));
        Assert.assertTrue("'AffiliationCompanySymbolsField' field is not displayed, or disabled.", mm.IsElementPresent(cd.AffiliationCompanySymbolsField));
        Assert.assertTrue("'AffiliationPositionEmployedField' field is not displayed, or disabled.", mm.IsElementPresent(cd.AffiliationPositionEmployedField));
        Assert.assertTrue("'ControlPersonPicklist' field is not displayed, or disabled.", mm.IsElementPresent(cd.ControlPersonPicklist));
        System.out.println("[INFO]: Test 'Check Fields On The Client Details Page' - PASSED.");
    }

    @Test
    public void CheckFieldsOnBuildHouseholdPage() throws InterruptedException {
        System.out.println("[INFO]: Test 'Check Fields On The Build Household Page' started >>>");
        bh.GoToBuildHouseholdPage();
        Assert.assertTrue("'RelatedLegalEntityField' field on 'Build Household' form is not displayed, or disabled.", mm.IsElementPresent(bh.RelatedLegalEntityField));
        Assert.assertTrue("'RelationshipPicklist' field on 'Build Household' form is not displayed, or disabled.", mm.IsElementPresent(bh.RelationshipPicklist));
        Assert.assertTrue("'IsImmediateFamilyCheckbox' checkbox on 'Build Household' form is not displayed, or disabled.", mm.IsElementPresent(bh.IsImmediateFamilyCheckbox));
        Assert.assertTrue("'IsDependentCheckbox' checkbox on 'Build Household' form is not displayed, or disabled.", mm.IsElementPresent(bh.IsDependentCheckbox));
        Assert.assertTrue("'IsBeneficiaryCheckbox' checkbox on 'Build Household' form is not displayed, or disabled.", mm.IsElementPresent(bh.IsBeneficiaryCheckbox));
        System.out.println("[INFO]: Test 'Check Fields On The Build Household Page' - PASSED.");
    }

    @Test
    public void CheckFieldsOnOpportunityPage() throws InterruptedException {
        System.out.println("[INFO]: Test 'Check Fields On The Opportunity Page' started >>>");
        opp.GoToOpportunityPage();
        Assert.assertTrue("'RecordTypeField' field on 'Opportunity' form is not displayed, or disabled.", mm.IsElementPresent(opp.RecordTypeField));
        Assert.assertTrue("'CarrierField' field on 'Opportunity' form is not displayed, or disabled.", mm.IsElementPresent(opp.CarrierField));
        Assert.assertTrue("'ProductField' field on 'Opportunity' form is not displayed, or disabled.", mm.IsElementPresent(opp.ProductField));
        Assert.assertTrue("'NameField' field on 'Opportunity' form is not displayed, or disabled.", mm.IsElementPresent(opp.NameField));
        Assert.assertTrue("'LegalEntityNameField' field on 'Opportunity' form is not displayed, or disabled.", mm.IsElementPresent(opp.LegalEntityNameField));
        Assert.assertTrue("'CloseDateField' field on 'Opportunity' form is not displayed, or disabled.", mm.IsElementPresent(opp.CloseDateField));
        Assert.assertTrue("'StageField' field on 'Opportunity' form is not displayed, or disabled.", mm.IsElementPresent(opp.StageField));
        opp.FillFieldsOnOpportunityPage();
        mm.NavigateToNextPage(opp.SmallSubmitButtonOnOpportunityPage, opp.LegalEntityField);
        Assert.assertTrue("'LegalEntityField' field on 'Opportunity' form is not displayed, or disabled.", mm.IsElementPresent(opp.LegalEntityField));
        Assert.assertTrue("'RolePicklist' field on 'Opportunity' form is not displayed, or disabled.", mm.IsElementPresent(opp.RolePicklist));
        System.out.println("[INFO]: Test 'Check Fields On The Opportunity Page' - PASSED.");
    }
}
