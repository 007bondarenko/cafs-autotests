package com.silverline.Cafs.Tests;

import com.silverline.Cafs.BaseTests.BaseTests;
import org.junit.Test;

import java.lang.*;

public class SmokeTests extends BaseTests{

    @Test
    public void CreateNewClient() throws InterruptedException {
        System.out.println("[INFO]: Test 'Create New Client' started >>>");
        //--->>>Navigate to ClientDetails Page
        //mm.WaitForHomePage();
        System.out.println("[INFO]: Navigate to 'Client Details' page.");
        cd.GoToClientDetailsPage();
        //--->>>Fill Form
        System.out.println("[INFO]: Fill fields on 'Client Details' page.");
        cd.FillFieldsOnClientDetailsPage();
        //--->>>Submit Form
        System.out.println("[INFO]: Submit 'Client Details' form.");
        mm.NavigateToNextPage(cd.SubmitButtonOnClientDetailsPage, cd.SolutationDataPageTwo);
        //---->>>Check Client Data on Page 2
        System.out.println("[INFO]: Check 'Client Details' data on Second page.");
        cd.CheckFieldsOnClientDetailsPage();
        //--->>>Go to Build Household
        System.out.println("[INFO]: Navigate to 'Build Household' page.");
        mm.NavigateToNextPage(cd.NextButtonOnClientDetailsPageTwo, bh.RelatedLegalEntityField);
        //--->>>Fill Build Household form
        System.out.println("[INFO]: Fill fields on 'Build Household' page.");
        bh.FillFieldsOnBuildHouseholdPage();
        //--->>>Go to Opportunity
        System.out.println("[INFO]: Navigate to 'Opportunity' page.");
        mm.NavigateToNextPage(bh.NextButtonOnBuildHouseholdPage, opp.RecordTypeField);
        //--->>>Fill Opportunity form
        System.out.println("[INFO]: Fill fields on 'Opportunity' page.");
        opp.FillFieldsOnOpportunityPage();
        //--->>>Go to Opportunity Roles
        System.out.println("[INFO]: Submit 'Opportunity' form.");
        mm.NavigateToNextPage(opp.SmallSubmitButtonOnOpportunityPage, opp.LegalEntityField);
        //--->>>Fill Opportunity Roles form
        System.out.println("[INFO]: Create new Opportunity Role.");
        opp.CreateNewOpportunityRole();
        //--->>>Navigate to Final Page
        System.out.println("[INFO]: Submit form.");
        mm.NavigateToNextPage(opp.BigSubmitButtonOnOpportunityPage, t.TotalBlock);
        //--->>>Check Data on Total Page
        System.out.println("[INFO]: Check data on 'Total' page.");
        t.CheckDataOnTotalPage();
        System.out.println("[INFO]: Test 'Create New Client' - PASSED.");
    }
}
