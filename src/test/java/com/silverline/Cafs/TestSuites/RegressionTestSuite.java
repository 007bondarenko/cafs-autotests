package com.silverline.Cafs.TestSuites;

import com.silverline.Cafs.Tests.CheckFields;
import com.silverline.Cafs.Tests.SmokeTests;
import com.silverline.Cafs.Tests.ValidationRulesTests;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SmokeTests.class,
        CheckFields.class,
        ValidationRulesTests.class
})

public class RegressionTestSuite{
}
